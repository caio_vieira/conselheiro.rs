/// Importação de bibliotecas.
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::env;

/// Criação de uma estrutura que define os tipos dos atributos
/// da resposta em json.
#[derive(Serialize, Deserialize, Debug)]
struct Advice {
    /// ID do conselho, definido pela API.
    id: i32,
    /// O conselho em si que será recebido pela API.
    advice: String,
}

/// Função principal. Deve ser assíncrona pois o método get
/// da biblioteca reqwest é assíncrono.
#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    // Declaração das variáveis.
    let mut request: Vec<HashMap<String, Advice>> = Vec::new(); // Para armazenar a resposta da requisição em um hashmap.
    let args: Vec<String> = env::args().collect(); // Para armazenar os argumentos da linha de comando.
    let iterations: i32;
    let mut advice: Vec<String> = Vec::new(); // Para armazenar a informação que será usada no programa.

    // Definição das variáveis.

    // Definindo a quantidade de conselhos.
    iterations = if args.len() > 1 {
        match args[1].parse::<i32>() {
            Ok(n) => n,
            Err(_) => 1,
        }
    } else {
        1
    };

    // Requisitando os conselhos.
    for _ in 0..iterations {
        request.push(
            reqwest::get("https://api.adviceslip.com/advice")
                .await?
                .json::<HashMap<String, Advice>>()
                .await?,
        );
    }

    for i in request {
        advice.push(i["slip"].advice.clone());
    }

    // Mostrando o conselho na tela.
    for c in advice {
        println!("{c}");
    }

    Ok(())
}
